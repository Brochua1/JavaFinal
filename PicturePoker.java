import java.util.Scanner;
import java.util.InputMismatchException;

public class PicturePoker {
	public static void main(String[] args) {
		final String DEALER_NAME = "Dylan";
		Scanner kb = new Scanner(System.in);
		Deck deck = new Deck();
		Dealer dealer = new Dealer(DEALER_NAME);
		int wins = 0;
		int losses = 0;
		int ante;
		int raise;
		int toFlip;
		int payout;
		double winner;
		String input;
		boolean endGame = false;
		boolean isSuccessful;
		boolean isComplete;

		System.out.println("Enter your name :");
		Player player = new Player(kb.nextLine());

		while (player.getBalance() >= 0 && !endGame) {
			player.dropHand();
			dealer.dropHand();
			deck.reset();
			deck.shuffle();
			winner = 0;
			payout = 0;
			raise = 0;
			toFlip = 0;
			input = "";

			ante = player.getMinBet();
			try {
				player.changeBalanceBy(-ante);
			} catch (IllegalArgumentException e) {
				System.out.println("Player has gone bankrupt!");
				break;
			}

			dealer.drawHand(deck);
			dealer.flipAllCards();

			player.drawHand(deck);
			player.sortHand();

			printGameInfo(player, dealer, ante);

			isSuccessful = false;
			while (!isSuccessful) {
				if (player.getBalance() >= 1) {
					System.out.println("By how much would you like to increase your bet ? (0-" + (5 - ante) + ")");
					try {
						raise = kb.nextInt();
						kb.nextLine();
						if (raise <= 5 - ante && raise >= 0) {
							player.changeBalanceBy(-raise);
							ante += raise;
							printGameInfo(player, dealer, ante);
							isSuccessful = true;
						} else 
							System.out.println("Input out of range (0-" + (5 - ante) + ")");
					} catch (IllegalArgumentException e) {
						System.out.println("Cannot bet more than your current balance: " + player.getBalance());
					} catch (InputMismatchException e) {
						System.out.println("Input must be an integer");
						kb.nextLine();
					}
				}
			}
			isComplete = false;
			while (!isComplete) {
				isSuccessful = false;
				while (!isSuccessful) {
					System.out.println(player);
					System.out.println(
							"Enter the number of each card you would like to exchange (separated by a space).");
					System.out.println("Entering the number of an already flipped card will flip it back");
					input = kb.nextLine();
					try {
						if(input.length() != 0){
							for (String s : input.split(" ")) {
								toFlip = Integer.parseInt(s);
								player.flipCardAt(toFlip);
							}
						}
						isSuccessful = true;
					} catch (NumberFormatException e) {
						System.out.println("All input must be integers");
					} catch (IllegalArgumentException e) {
						System.out.println(
								"Number " + toFlip + " is invalid must be in range (0-" + (player.getHandSize() - 1) + ")");
					}
				}
				System.out.println(player);
				System.out.println(
						"Enter 'confirm' to confirm your selection (Hidden cards will be swapped), enter 'cancel' to go back");
				// If input does not match 'confirm' send user back even if input != cancel
				// (Cancel is used for clarity but is never checked)
				isComplete = kb.nextLine().equalsIgnoreCase("confirm");
			}
			tradeAllFlippedCards(player, deck);

			printGameInfo(player, dealer, ante);

			player.sortHand();
			dealer.sortHand();

			dealer.flipAllCards();
			printGameInfo(player, dealer, ante);

			winner = determineWinner(player, dealer);
			if (winner > 0) {
				payout = (int) Math.floor(winner) / 10 * ante;
				System.out.println("Player " + player.getName() + " has won " + payout + " coins!!");
				player.changeBalanceBy(payout);
				player.incrementStars();
				wins++;
			} else if (winner < 0) {
				System.out.println("Dealer " + dealer.getName() + " has won " + ante + " coins!!");
				player.decrementStars();
				losses++;
			} else {
				System.out.println("It's a draw! Bets are returned");
				player.changeBalanceBy(ante);
			}
			input = "";
			isSuccessful = false;
			while (!isSuccessful) {
				System.out.println("Would you like to play again? (yes/no)");
				input = kb.nextLine();
				if (input.equalsIgnoreCase("n") || input.equalsIgnoreCase("no")) {
					isSuccessful = true;
					endGame = true;
				} else if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes"))
					isSuccessful = true;
			}
		}
		System.out.println("Exiting game!");
		System.out.println("You've won " + wins + " times and lost " + losses + " times");
		System.out.println("You finished with " + player.getBalance() + " coins!");
	}

	public static void printGameInfo(Player p, Dealer d, int ante) {
		System.out.println("\nBet: " + ante + "\n");
		System.out.println(d);
		System.out.println(p);
	}
	
	public static void printFinalGameInfo(Player p, double pStrength, Dealer d, double dStrength){
		System.out.println(d);
		System.out.println("Dealer got: " + determineTypeOfHand(dStrength));
		System.out.println("\n" + p);
		System.out.println("You got: " + determineTypeOfHand(pStrength));
	}
	
	//Returns a String representation of the hand strength based on the number it gets (Hand.getStrength())
	/*
	 *First digit shows level of the hand
	 *Second digit shows strength of the most common card in that hand
	 *Third digit shows strength of the 2nd most common card in that hand
	 *5 of a kind 	= 160
	 *4 of a kind 	= 80
	 *Full house 	= 60
	 *Triple 		= 40
	 *Two Pairs		= 30
	 *Pair			= 20
	 *High Card		= 10
	 */
	public static String determineTypeOfHand(double strength){
		switch ((int) Math.floor(strength) / 10){
			case 16:
				return "Five of a Kind";
			case 8:
				return "Four of a Kind";
			case 6:
				return "Full House";
			case 4:
				return "Three of a Kind";
			case 3:
				return "Double Pairs";
			case 2:
				return "Pair";
			case 1:
				return "High Card";
		}
		throw new IllegalArgumentException("Unknown hand for strength " + strength);
	}

	public static void tradeAllFlippedCards(Player p, Deck d) {
		for (int i = 0; i < p.getHandSize(); i++)
			if (p.getCard(i).getIsFaceDown())
				p.exchangeCard(d.drawTopCard(), i);
	}

	public static double determineWinner(Player p, Dealer d) {
		double pStrength = p.getHandStrength();
		double dStrength = d.getHandStrength();
		printFinalGameInfo(p, pStrength, d, dStrength);
		if (pStrength > dStrength)
			return pStrength;
		else if (dStrength > pStrength)
			return -dStrength;
		else
			return 0;
	}
}