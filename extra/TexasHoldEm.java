import java.util.Scanner;
import java.util.InputMismatchException;

public class TexasHoldEm{
	public static void main(String[] args){
		final int CLEAR_SCREEN_LINES = 50;
		Scanner kb = new Scanner(System.in);
		Table table = new Table();
		Deck deck = new Deck();
		int ties = 0;
		int roundsPlayed = 0;
		int bigBlind = 0;
		int smallBlind = 1;
		int raise;
		int winner;
		int payout;
		String input;
		boolean endGame = false;
		boolean isSuccessful;
		boolean isComplete;
		
		testClearSettings(CLEAR_SCREEN_LINES, kb);
		
		System.out.println("Player 1, Enter your name :");
		Player p1 = new Player(kb.nextLine());
		System.out.println("Player 2, Enter your name :");
		Player p2 = new Player(kb.nextLine());
		//used to make the player rotation
		Player[] players = {p1, p2};
		
		
		
		while(p1.getBalance() > 0 && p2.getBalance() > 0 && !endGame){
			roundsPlayed++;
			resetPlayer(p1);
			resetPlayer(p2);
			table.reset();
			deck.reset();
			deck.shuffle();
			winner = 0;
			payout = 0;
			raise = 0;
			input = "";
			
			//rotates which player has which role every other round
			if(roundsPlayed % 2 == 0){
				bigBlind = 0;
				smallBlind = 1;
			}else{
				bigBlind = 1;
				smallBlind = 0;
			}
				
			//Increases the minimum bet every 4 rounds
			table.setAnte(roundsPlayed / 4 * 100 + 100);
			
			//set small blind's bet to half ante
			try{
				players[smallBlind].changeBetBy(table.getAnte() / 2);
			}catch(IllegalArgumentException e){
				//if small blind bal < ante puts them all in automatically
				players[smallBlind].changeBetBy(players[smallBlind].getBalance());
				System.out.println(players[smallBlind].getName() + ", you cannot afford the mandatory minimum bet, you've gone all in");
			}
			//These will never cause an errors because the previous trycatch forces arguments to be valid
			players[smallBlind].changeBalanceBy(-players[smallBlind].getCurrentBet());
			table.changePotBy(players[smallBlind].getCurrentBet());
			
			//set big blind's bet to whole ante
			try{
				players[bigBlind].changeBetBy(table.getAnte());
			}catch(IllegalArgumentException e){
				//if small blind bal < ante puts them all in automatically
				players[bigBlind].changeBetBy(players[bigBlind].getBalance());
				System.out.println(players[bigBlind].getName() + ", you cannot afford the mandatory minimum bet, you've gone all in");
				players[bigBlind].setIsAllIn(true);
				refundOnAllIn(players, table);
			}
			//These will never cause an errors because the previous trycatch forces arguments to be valid
			players[bigBlind].changeBalanceBy(-players[bigBlind].getCurrentBet());
			table.changePotBy(players[bigBlind].getCurrentBet());
			
			players[smallBlind].drawHand(deck);
			players[smallBlind].flipAllCards();
			players[bigBlind].drawHand(deck);
			
			switchToNextPlayer(kb, players[bigBlind], players[smallBlind], CLEAR_SCREEN_LINES);
			
			if(!players[smallBlind].getIsAllIn() && !players[bigBlind].getIsAllIn()){
				System.out.println("1");
				getBet(players, smallBlind, table, kb);
				if(players[smallBlind].getHasFolded()){
					payWinner(players[bigBlind], table);
					continue;
				}
				switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
				getBet(players, bigBlind, table, kb);
				if(players[bigBlind].getHasFolded()){
					payWinner(players[smallBlind], table);
					continue;
				}else if(!players[bigBlind].getIsAllIn()){
					do{
						switchToNextPlayer(kb, players[bigBlind], players[smallBlind], CLEAR_SCREEN_LINES);
						getBet(players, smallBlind, table, kb);
						if(players[smallBlind].getHasFolded() || players[smallBlind].getIsAllIn()){
							break;
						}
						if(players[smallBlind].getCurrentBet() == players[bigBlind].getCurrentBet())
							break;
						switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
						getBet(players, bigBlind, table, kb);
						if(players[bigBlind].getHasFolded() || players[bigBlind].getIsAllIn()){
							break;
						}
					}while(players[smallBlind].getCurrentBet() != players[bigBlind].getCurrentBet());
					
					if(players[smallBlind].getHasFolded()){
						System.out.println(players[bigBlind].getName() + " has folded");
						payWinner(players[bigBlind], table);
						printGameInfo(players, table);
						continue;
					}else if(players[bigBlind].getHasFolded()){
						System.out.println(players[smallBlind].getName() + " has folded");
						payWinner(players[smallBlind], table);
						printGameInfo(players, table);
						continue;
					}
				}
			}
			table.addCard(deck.drawTopCard());
			table.addCard(deck.drawTopCard());
			table.addCard(deck.drawTopCard());
			
			if(!players[smallBlind].getIsAllIn() && !players[bigBlind].getIsAllIn()){
				System.out.println("2");
				players[smallBlind].changeBetBy(-players[smallBlind].getCurrentBet());
				players[bigBlind].changeBetBy(-players[bigBlind].getCurrentBet());
				table.setAnte(roundsPlayed / 4 * 100 + 100);
				
				switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
				getBet(players, bigBlind, table, kb);
				if(players[bigBlind].getHasFolded()){
					payWinner(players[smallBlind], table);
					continue;
				}else if(!players[smallBlind].getIsAllIn()){
					do{
						switchToNextPlayer(kb, players[bigBlind], players[smallBlind], CLEAR_SCREEN_LINES);
						getBet(players, smallBlind, table, kb);
						if(players[smallBlind].getHasFolded() || players[smallBlind].getIsAllIn()){
							break;
						}
						if(players[smallBlind].getCurrentBet() == players[bigBlind].getCurrentBet())
							break;
						switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
						getBet(players, bigBlind, table, kb);
						if(players[bigBlind].getHasFolded() || players[bigBlind].getIsAllIn()){
							break;
						}
					}while(players[smallBlind].getCurrentBet() != players[bigBlind].getCurrentBet());
					if(players[smallBlind].getHasFolded()){
						System.out.println(players[bigBlind].getName() + " has folded");
						payWinner(players[bigBlind], table);
						printGameInfo(players, table);
						continue;
					}else if(players[bigBlind].getHasFolded()){
						System.out.println(players[smallBlind].getName() + " has folded");
						payWinner(players[smallBlind], table);
						printGameInfo(players, table);
						continue;
					}
				}
			}
			
			table.addCard(deck.drawTopCard());
			
			if(!players[smallBlind].getIsAllIn() && !players[bigBlind].getIsAllIn()){
				System.out.println("3");
				players[smallBlind].changeBetBy(-players[smallBlind].getCurrentBet());
				players[bigBlind].changeBetBy(-players[bigBlind].getCurrentBet());
				table.setAnte(roundsPlayed / 4 * 100 + 100);
				
				switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
				getBet(players, bigBlind, table, kb);
				if(players[bigBlind].getHasFolded()){
					payWinner(players[smallBlind], table);
					continue;
				}else if(!players[smallBlind].getIsAllIn()){
					do{
						switchToNextPlayer(kb, players[bigBlind], players[smallBlind], CLEAR_SCREEN_LINES);
						getBet(players, smallBlind, table, kb);
						if(players[smallBlind].getHasFolded() || players[smallBlind].getIsAllIn()){
							break;
						}
						if(players[smallBlind].getCurrentBet() == players[bigBlind].getCurrentBet())
							break;
						switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
						getBet(players, bigBlind, table, kb);
						if(players[bigBlind].getHasFolded() || players[bigBlind].getIsAllIn()){
							break;
						}
					}while(players[smallBlind].getCurrentBet() != players[bigBlind].getCurrentBet());
					if(players[smallBlind].getHasFolded()){
						System.out.println(players[bigBlind].getName() + " has folded");
						payWinner(players[bigBlind], table);
						printGameInfo(players, table);
						continue;
					}else if(players[bigBlind].getHasFolded()){
						System.out.println(players[smallBlind].getName() + " has folded");
						payWinner(players[smallBlind], table);
						printGameInfo(players, table);
						continue;
					}
				}
			}

			table.addCard(deck.drawTopCard());
			
			if(!players[smallBlind].getIsAllIn() && !players[bigBlind].getIsAllIn()){
				players[smallBlind].changeBetBy(-players[smallBlind].getCurrentBet());
				players[bigBlind].changeBetBy(-players[bigBlind].getCurrentBet());
				table.setAnte(roundsPlayed / 4 * 100 + 100);
				
				switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
				getBet(players, bigBlind, table, kb);
				if(players[bigBlind].getHasFolded()){
					payWinner(players[smallBlind], table);
					continue;
				}else if(!players[smallBlind].getIsAllIn()){
					do{
						switchToNextPlayer(kb, players[bigBlind], players[smallBlind], CLEAR_SCREEN_LINES);
						getBet(players, smallBlind, table, kb);
						if(players[smallBlind].getHasFolded() || players[smallBlind].getIsAllIn()){
							break;
						}
						if(players[smallBlind].getCurrentBet() == players[bigBlind].getCurrentBet())
							break;
						switchToNextPlayer(kb, players[smallBlind], players[bigBlind], CLEAR_SCREEN_LINES);
						getBet(players, bigBlind, table, kb);
						if(players[bigBlind].getHasFolded() || players[bigBlind].getIsAllIn()){
							break;
						}
					}while(players[smallBlind].getCurrentBet() != players[bigBlind].getCurrentBet());
					
					if(players[smallBlind].getHasFolded()){
						System.out.println(players[bigBlind].getName() + " has folded");
						payWinner(players[bigBlind], table);
						printGameInfo(players, table);
						continue;
					}else if(players[bigBlind].getHasFolded()){
						System.out.println(players[smallBlind].getName() + " has folded");
						payWinner(players[smallBlind], table);
						printGameInfo(players, table);
						continue;
					}
				}
			}
			players[smallBlind].setIsFaceDownWholeHand(false);
			players[bigBlind].setIsFaceDownWholeHand(false);
			
			table.sort();
			printGameInfo(players, table);
			
			System.out.println("Hit 'enter' to show the best hands!");
			kb.nextLine();
			System.out.println("\n\n\n\n");
			
			printSelectedHands(players, table);
			
			//if winner = 1 smallBlind wins, if winner = -1 bigBlind wins, if winner = 0 its a complete tie
			winner = getWinner(players[smallBlind], players[bigBlind]);
			if(winner == 1){
				System.out.println(players[smallBlind].getName() + " won!");
				payWinner(players[smallBlind], table);
			}else if(winner == -1){
				System.out.println(players[bigBlind].getName() + " won!");
				payWinner(players[bigBlind], table);
			}else{
				System.out.println("It's a tie! Bets are returned to each player");
				splitPayout(players, table);
			}
			
			isSuccessful = false;
			while (!isSuccessful) {
				System.out.println("Would you like to play again? (yes/no)");
				input = kb.nextLine();
				if (input.equalsIgnoreCase("n") || input.equalsIgnoreCase("no")) {
					isSuccessful = true;
					endGame = true;
				} else if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes"))
					isSuccessful = true;
			}
		}
		
		System.out.println("Exiting game!");
		System.out.println(p1.getName() + " won " + p1.getGamesWon() + " out of " + roundsPlayed + " and finished with a balance of " + p1.getBalance());
		System.out.println(p2.getName() + " won " + p2.getGamesWon() + " out of " + roundsPlayed + " and finished with a balance of " + p2.getBalance());
	}
	
	public static void resetPlayer(Player p){
		p.setIsAllIn(false);
		p.dropHand();
		p.setHasFolded(false);
		p.changeBetBy(-p.getCurrentBet());
		p.setHandStrength(0);
	}
	
	public static void payWinner(Player winner, Table t){
		winner.incrementGamesWon();
		winner.changeBalanceBy(t.getPot());
	}
	
	public static void splitPayout(Player[] players, Table t){
		players[0].changeBalanceBy(t.getPot() / 2);
		players[1].changeBalanceBy(t.getPot() / 2);
	}
	
	public static void getBet(Player[] players, int whichPlayer, Table t, Scanner kb){
		Player p = players[whichPlayer];
		boolean isSuccessful = false;
		int raise = 0;
		printGameInfo(players, t);
		while (!isSuccessful) {
			if (p.getBalance() > (t.getAnte() - p.getCurrentBet())) {
				System.out.println(p.getName() + " how much would you like to increase your bet by?");
				System.out.println("Range : " + (t.getAnte() - p.getCurrentBet()) + " - " + (p.getBalance()) + " or -1 to fold");
				if(players[0].getCurrentBet() == players[1].getCurrentBet())
					System.out.println("Enter 0 to check");
				try {
					raise = kb.nextInt();
					kb.nextLine();
					if(raise == -1){
						raise = 0;
						p.setHasFolded(true);
						isSuccessful = true;
					}else if (raise <= p.getBalance() && raise >= (t.getAnte() - p.getCurrentBet())) {
						t.setAnte(p.getCurrentBet() + raise);
						p.changeBetBy(raise);
						p.changeBalanceBy(-raise);
						isSuccessful = true;
					}else if(raise == 0 && players[0].getCurrentBet() == players[1].getCurrentBet()){
						isSuccessful = true;
					}else 
						System.out.println("Input out of range (" + (t.getAnte() - p.getCurrentBet()) + "-" + p.getBalance() + ") or -1");
				}catch(IllegalArgumentException e){
					System.out.println("Cannot bet more than your current balance: " + p.getBalance());
				}catch(InputMismatchException e){
					System.out.println("Input must be an integer");
					kb.nextLine();
				}
			}else{
				System.out.println("You must go all in or fold (-1 to fold or " + p.getBalance() + ")");
				try{
					raise = kb.nextInt();
					kb.nextLine();
					if(raise == -1){
						raise = 0;
						p.setHasFolded(true);
						isSuccessful = true;
					}else if(raise == p.getBalance()) {
						p.changeBetBy(raise);
						p.changeBalanceBy(-raise);
						p.setIsAllIn(true);
						refundOnAllIn(players, t);
						isSuccessful = true;
					}else 
						System.out.println("Input out of range " + p.getBalance() + " or -1");
				}catch(InputMismatchException e){
					System.out.println("Input must be an integer");
					kb.nextLine();
				}
			}
		}
		t.changePotBy(raise);
	}
	
	public static void refundOnAllIn(Player[] players, Table t){
		Player toRefund = (!players[0].getIsAllIn()) ? players[0]: players[1];
		Player allIn = (players[0].getIsAllIn()) ? players[0]: players[1];
		
		toRefund.changeBalanceBy(toRefund.getCurrentBet() - allIn.getCurrentBet());
		toRefund.changeBetBy(-(toRefund.getCurrentBet() - allIn.getCurrentBet())); 
		t.changePotBy(-(toRefund.getCurrentBet() - allIn.getCurrentBet()) );
	}
	
	public static void printGameInfo(Player[] players, Table t){
		System.out.println("\nPot : " + t.getPot());
		System.out.println("Ante : " + t.getAnte() + "\n");
		System.out.println(players[0]);
		System.out.println("Table :\n" + t);
		System.out.println(players[1]);
	}
	
	public static void printSelectedHands(Player[] players, Table t){
		DynamicCardArray bestHand = t.getBestHand(players[0]);
		System.out.println("\n" + players[0].getName() + "'s best possible hand :");
		System.out.println(determineTypeOfHand(players[0].getHandStrength()));
		System.out.print(bestHand);
		bestHand = t.getBestHand(players[1]);
		System.out.println("\n" + players[1].getName() + "'s best possible hand :");
		System.out.println(determineTypeOfHand(players[1].getHandStrength()));
		System.out.print(bestHand);
	}
	
	public static void testClearSettings(int CLEAR_SCREEN_LINES, Scanner kb){
		DynamicCardArray testCardArray = new DynamicCardArray();
		for(int i = 0; i < 5; i++)
			testCardArray.add(new Card(Value.ACE, Suit.HEARTS));
		
		System.out.println("You should not be able to see this line");
		for(int i = 0; i < CLEAR_SCREEN_LINES; i++)
			System.out.println("");
		System.out.println("Adjust the vertical size of the terminal until you can't see the statement above, once done hit 'enter'");
		kb.nextLine();
		boolean isSuccessful = false;
		while(!isSuccessful){
			System.out.println(testCardArray);
			System.out.println("Adjust the horizontal size of the terminal until you can see the hand printed properly (on one line)");
			System.out.println("type 'reload' to print the hand again and 'confirm' once done");
			if(kb.nextLine().equalsIgnoreCase("confirm"))
				isSuccessful = true;
		}
	}
	
	public static void switchToNextPlayer(Scanner kb, Player playing, Player nextPlayer, int CLEAR_SCREEN_LINES){
		//Prints this many blank lines when switching between players
		
		playing.flipAllCards();
		nextPlayer.flipAllCards();
		for(int i = 0; i < CLEAR_SCREEN_LINES; i++)
			System.out.println("");
		System.out.println(playing.getName() + ", please look away from the screen");
		System.out.println(nextPlayer.getName() + ", hit enter to start your turn");
		kb.nextLine();
		System.out.println(nextPlayer.getName() + "'s turn:");
	}
	
	public static int getWinner(Player p1, Player p2){
		if(p1.getHandStrength() > p2.getHandStrength())
			return 1;
		else if (p1.getHandStrength() < p2.getHandStrength())
			return -1;
		else
			return 0;
	}
	
	//Returns a String representation of the hand strength based on the number it gets (Hand.getStrength())
	/*
	 *First and second digit shows level of the hand
	 *Third and Fourth digits show strength of the main card in the hand (Usually Highest)
	 *Fifth and Sixth digit shows strength of the secondary card in the hand (Usually Secondary pair)
	 *Decimals shows strength of filler cards to complete the hand of 5
	 *Royal Flush 		= 200 000
	 *Straight Flush	= 180 000
	 *Four of a Kind	= 160 000
	 *Full House 		= 140 000
	 *Flush				= 120 000
	 *Straight			= 100 000
	 *Three of a Kind	=  80 000
	 *Two Pairs			=  60 000
	 *Pairs				=  40 000
	 *High Card			=  20 000
	 */
	public static String determineTypeOfHand(double strength){
		switch ((int) Math.floor(strength) / 10000){
			case 20:
				return "Royal Flush";
			case 18:
				return "Straight Flush";
			case 16:
				return "Four of a Kind";
			case 14:
				return "Full House";
			case 12:
				return "Flush";
			case 10:
				return "Straight";
			case 8:
				return "Three of a Kind";
			case 6:
				return "Two Pairs";
			case 4:
				return "Pair";
			case 2:
				return "High Card";
		}
		throw new IllegalArgumentException("Unknown hand for strength " + strength);
	}
}