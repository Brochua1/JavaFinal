public class Card {
	private final String CARD_BACK = "XXXXX";
    private Suit suit;
    private Value value;
	private boolean isFaceDown;

    //Constructor setting the suit and value based on the suit
    Card(Value v,Suit s){
        this.suit = s;
        this.value = v;
		this.isFaceDown = false;
    }

    //getters
    public Suit getSuit() {
        return this.suit;
    }
    
    public Value getValue() {
		return this.value;
	}
	
	public int getStrengthValue(){
		return this.value.getValue();
	}
	
	public boolean getIsFaceDown(){
		return this.isFaceDown;
	}
	
	//Flips the card, if it is face up it makes it face down and vice versa
	public void flipCard(){
		this.isFaceDown = !this.isFaceDown;
	}

    //toString override
    public String toString(){
		if (isFaceDown)
			return CARD_BACK;
        return this.value.toString() + this.suit.toString();
    }
}
