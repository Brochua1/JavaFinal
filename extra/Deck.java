import java.util.Random;

public class Deck {
    private final static Value[] values = Value.values();
    private final static int SHUFFLE_CONSTANT = 52;
	private static Random rng = new Random();
	public DynamicCardArray cards;
	
	//Make a deck with 5x each picture
    public Deck(){
    	cards = new DynamicCardArray();
        this.buildDeck();
    }
    
    //Reset the deck to its default state (See constructor)
    public void reset() {
    	this.cards.empty();
    	this.buildDeck();
    }
    
    //Shuffles the deck by making SHUFFLE_CONSTANT card permutations
    public void shuffle(){
        Card temp;
        int pos1, pos2;
        for(int i = 0; i < SHUFFLE_CONSTANT; i++){
            pos1 = rng.nextInt(this.cards.size());
            pos2 = rng.nextInt(this.cards.size());
            temp = this.cards.get(pos1);
            this.cards.set(this.cards.get(pos2), pos1);
            this.cards.set(temp, pos2);
        }
    }
    
    //Returns the top card and removes it from the deck
    public Card drawTopCard() {
    	Card c = this.cards.get(0);
    	this.cards.remove(0);
    	return c;
    }
    
    //adds a card to the end of the deck
    public void addCard(Card c) {
    	this.cards.add(c);
    }
    
    public String toString() {
    	return this.cards.toString();
    }
	
	private void buildDeck(){
		for(Suit s : Suit.values()){
            for(int i = 1; i < values.length; i++)
                this.cards.add(new Card(values[i], s));
        }
	}
}
