public enum Value {
    LOWACE(-1){
        public String toString(){
            return "A";
        }
    },
	TWO(0){
        public String toString() {
            return "2";
        }
    },
	THREE(1){
        public String toString() {
            return "3";
        }
    },
	FOUR(2){
        public String toString(){
            return "4";
        }
    },
	FIVE(3){
        public String toString() {
            return "5";
        }
    },
	SIX(4){
        public String toString() {
            return "6";
        }
    },
	SEVEN(5){
        public String toString() {
            return "7";
        }
    },
	EIGHT(6){
        public String toString() {
            return "8";
        }
    },
	NINE(7){
        public String toString() {
            return "9";
        }
    },
	TEN(8){
        public String toString() {
            return "10";
        }
    },
	JACK(9){
        public String toString() {
            return "J";
        }
    },
	QUEEN(10){
        public String toString() {
            return "Q";
        }
    },
	KING(11){
        public String toString() {
            return "K";
        }
    },
	ACE(12){
        public String toString() {
            return "A";
        }
    };
	
	private final int value;
	
	private Value(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
}
