public class Player{
	private static final int DEFAULT_BAL = 1000;
	private static final int HAND_SIZE = 2;
	private int balance;
	private int currentBet;
	private int gamesWon;
	private Hand hand;
	private double handStrength;
	private String name;
	private boolean hasFolded;
	private boolean isAllIn;

	
	//constructor
	public Player(String name){
		this.name = name;
		this.hand = new Hand();
		this.balance = DEFAULT_BAL;
		this.handStrength = 0;
		this.currentBet = 0;
		this.isAllIn = false;
		this.hasFolded = false;
	}
	
	//getters
	public int getBalance(){
		return this.balance;
	}

	public String getName(){
		return this.name;
	}
	
	public int getCurrentBet(){
		return this.currentBet;
	}
	
	public Card getCard(int i){
		return this.hand.getCard(i);
	}
	
	public int getHandSize(){
		return this.hand.getHandSize();
	}
	
	public boolean getHasFolded(){
		return this.hasFolded;
	}
	
	public int getGamesWon(){
		return this.gamesWon;
	}
	
	public boolean getIsAllIn(){
		return this.isAllIn;
	}
	
	public double getHandStrength(){
		return this.handStrength;
	}
	
	//setters
	public void setHandStrength(double strength){
		if(strength < 0)
			throw new IllegalArgumentException("Strength should be greater or equal to 0, got " + strength);
		this.handStrength = strength;
	}
	
	public void setIsAllIn(boolean isAllIn){
		this.isAllIn = isAllIn;
	}
	
	public void setIsFaceDownWholeHand(boolean isFaceDown){
		for(int i = 0; i < hand.getHandSize(); i++)
			if(hand.getCard(i).getIsFaceDown() != isFaceDown)
				hand.getCard(i).flipCard();
	}
	
	
	public void setHasFolded(boolean hasFolded){
		this.hasFolded = hasFolded;
	}
	
	//adds a value to the balance as long as it wouldnt make it negative
	public void changeBalanceBy(int toAdd){
		if(this.balance + toAdd < 0)
			throw new IllegalArgumentException("Balance cannot become negative");
		this.balance += toAdd;
	}
	
	public void changeStrengthBy(double toAdd){
		if(this.handStrength + toAdd < 0)
			throw new IllegalArgumentException("Strength cannot become negative");
		this.handStrength += toAdd;
	}
	
	public void changeBetBy(int toAdd){
		if(this.currentBet + toAdd < 0 || this.balance - toAdd < 0)
			throw new IllegalArgumentException("Current bet cannot become negative or make balance negative");
		this.currentBet += toAdd;
	}

	//Custom methods
	//Draws HAND_SIZE cards from origin deck
	public void drawHand(Deck origin){
		for(int i = 0; i < HAND_SIZE; i++)
			hand.addCard(origin.drawTopCard());
	}
	
	public void incrementGamesWon(){
		this.gamesWon++;
	}
	
	//shorts player's hand
	public void sortHand(){
		this.hand.sort();
	}
	
	//Empties the player's hand
	public void dropHand(){
		this.hand.empty();
		this.handStrength = 0;
	}
	
	//Replaces the card at a given index with a new one
	public void exchangeCard(Card c, int i){
		this.hand.replaceCardAt(c, i);
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		this.hand.flipCardAt(i);
	}
	
	//Flips all cards in hand
	public void flipAllCards(){
		this.hand.flipAllCards();
	}
	
	public String toString(){
		return this.name + " - " + this.balance + " Coins - Current bet : " + this.currentBet + "\n" + this.hand;
	}
}