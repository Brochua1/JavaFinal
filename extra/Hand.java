public class Hand {
	private DynamicCardArray cards;
	
	public Hand() {
		this.cards = new DynamicCardArray();
	}
	
	//returns card at specified index
	public Card getCard(int i) {
		return this.cards.get(i);
	}
	
	public int getHandSize(){
		return cards.size();
	}
	
	//removes card at a given index and shift all other cards to fill in the gap
	public void removeCard(int i) {
		this.cards.remove(i);
	}
	
	//adds a card to the end of the hand
	public void addCard(Card c) {
		this.cards.add(c);
	}
	
	//Empties hand
	public void empty() {
		this.cards.empty();
	}
	
	//Puts a given card at a given index
	public void replaceCardAt(Card c, int i){
		cards.set(c, i);
	}
	
	//sorts the cards by strength
	public void sort(){
		this.cards.sort();
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		this.cards.flipCardAt(i);
	}
	
	//Flips all cards in hand
	public void flipAllCards(){
		for(int i = 0; i < this.cards.size(); i++)
			this.cards.flipCardAt(i);
	}

	public String toString() {
		return this.cards.toString();
	}
}
