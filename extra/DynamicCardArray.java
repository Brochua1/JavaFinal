public class DynamicCardArray{
	private static final int CARD_WIDTH = 7;
	private static final int CARD_HEIGHT = 6;
	private static final String HIDDEN_CARD_INFO = "X";
    private Card[] array;
	private int size;

	//Creates the array with a very large size and sets size to 0
	public DynamicCardArray(){
		this.array = new Card[10000];
		this.size = 0;
	}
	
	//Add a given value to the end of array, updating the size
	public void add(Card value){
		if(this.size == this.array.length)
			this.doubleArrayLength();
		this.array[this.size] = value;
		this.size++;
	}
	
	//Return the value for a given index of the array
	public Card get(int i){
		if(i < this.size && i >= 0)
			return this.array[i];
		throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
	}
	
	//set the card at a given index to a given card
	public void set(Card c, int i){
		if(!(i < this.size && i >= 0))
			throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
		this.array[i] = c;
	}
	
	//remove card at a given index and reduce the size accordingly
	public void remove(int i) {
		this.size--;
		for(int j = i; j < this.size; j++)
			this.array[j] = this.array[j + 1];
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		if(!(i < this.size && i >= 0))
			throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
		this.array[i].flipCard();
	}
	
	//Return the size of the array
	public int size(){
		return this.size;
	}
	
	//Empties the array
	public void empty() {
		for(int i = 0; i < size; i++)
			this.array[i] = null;
		this.size = 0;
	}
	
	//Appends all contents of given DynamicCardArray to the one its called on
	public void merge(DynamicCardArray d){
		for(int i = 0; i < d.size(); i++)
			this.add(d.get(i));
	}
	
	//counts how many occurrences of a given picture there are
	public int countSuit(Suit s) {
		int count = 0;
		for(int i = 0; i < this.size; i++) {
			if(array[i].getSuit() == s)
				count++;
		}
		return count;
	}
	
	//Checks if a value is contained in the array
	public boolean contains(Card c){
		for(int i = 0; i < this.size; i++)
			if(this.array[i].getSuit() == c.getSuit() && this.array[i].getValue() == c.getValue())
				return true;
		return false;
	}
	
	//Doubles the length of the array
	private void doubleArrayLength(){
		Card[] newArr = new Card[this.array.length * 2];
		for(int i = 0; i < this.size; i++){
			newArr[i] = array[i];
		}
		
		this.array = newArr;
	}
	
	public void sort(){
		int posOfMin;
		Card temp = null;

		for(int j = 0; j < this.size - 1; j++){
			posOfMin = j;
			for(int i = j + 1; i < this.size; i++)
				if (this.array[i].getStrengthValue() < this.array[posOfMin].getStrengthValue())
					posOfMin = i;

			temp = this.array[j];
			this.array[j] = this.array[posOfMin];
			this.array[posOfMin] = temp;
		}
	}

	public DynamicCardArray copy(){
		DynamicCardArray copy = new DynamicCardArray();
		for(int i = 0; i < this.size; i++)
			copy.add(this.get(i));
		return copy;
	}
	
	public String toString() {
		String toReturn = "";
		for(int j = 0; j <= CARD_HEIGHT; j++) {
			for(int i = 0; i < this.size; i++) 
				if(j == 0 || j == CARD_HEIGHT) 
					toReturn += this.drawCardEndCaps() + " ";
				else if(j == 1 || j == (CARD_HEIGHT - 1)) 
					toReturn += this.drawLineWithValue(i, (j == 1)) + " ";
				else if(j == CARD_HEIGHT / 2) 
					toReturn += this.drawCardSuit(i) + " ";
				else 
					toReturn += this.drawCardFiller() + " ";
			
			toReturn += "\n";
		}
		return toReturn;
	}
	
	//return a string of the line containing the suit of the card
	private String drawCardSuit(int i) {
		String toReturn = "";
		
		toReturn += "|";
		for(int w = 0; w < CARD_WIDTH; w++) {
			if(w == CARD_WIDTH / 2)
				if(this.array[i].getIsFaceDown())
					toReturn += HIDDEN_CARD_INFO;
				else
					toReturn += this.array[i].getSuit();
			else
				toReturn += ".";
		}
		toReturn += "|";
		return toReturn;
	}
	
	//return a string of a line used as filler (filled with dots)
	private String drawCardFiller() {
		String toReturn = "";
		toReturn += "|";
		for(int w = 0; w < CARD_WIDTH; w++)
			toReturn += ".";
		toReturn += "|";
		
		return toReturn;
	}

	//Returns a String representation of the top and bottom of the cards
	private String drawCardEndCaps() {
		String toReturn = "";
		
		toReturn += "+";
		for(int w = 0; w < CARD_WIDTH; w++)
			toReturn += "-";
		toReturn += "+";
		return toReturn;
	}
	
	//Draws the line containing the value of the card. It finds the card in the array using a given index and puts the number on
	//the right side depending on if its the top or bottom line
	private String drawLineWithValue(int i, boolean isTopLine) {
		String toReturn = "";
		
		toReturn += "|";
		if(isTopLine){
			if(this.array[i].getIsFaceDown())
				toReturn += HIDDEN_CARD_INFO;
			else
				toReturn += this.array[i].getValue();
		}
		//Sets w to 0 or 1 depending on if the value is a 2 digit number (TEN)
		for(int w = (this.array[i].getValue() == Value.TEN && !(this.array[i].getIsFaceDown())) ? 1 : 0; w < (CARD_WIDTH - 1); w++) 
			toReturn += ".";
		
		if(!isTopLine){
			if(this.array[i].getIsFaceDown())
				toReturn += HIDDEN_CARD_INFO;
			else
				toReturn += this.array[i].getValue();
		}
		toReturn += "|";
		
		return toReturn;
	}
}