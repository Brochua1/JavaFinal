public class Table{
	private Hand cards;
	private int ante;
	private int pot; //holds the bets
	
	public Table(){
		pot = 0;
		ante = 100;
		cards = new Hand();
	}
	
	public int getAnte(){
		return this.ante;
	}
	
	public int getPot(){
		return this.pot;
	}
	//setters
	public void setAnte(int newAnte){
		this.ante = newAnte;
	}
	
	public void addCard(Card c){
		cards.addCard(c);
	}
	
	public void changePotBy(int change){
		if(this.pot + change < 0)
			throw new IllegalArgumentException("Pot cannot become negative");
		this.pot += change;
	}
	
	public void reset(){
		this.cards.empty();
		this.pot = 0;
	}
	
	public void sort(){
		this.cards.sort();
	}
	
	public String toString(){
		return this.cards.toString();
	}
	
	/*returns the best hand for a player object and sets its strength in the Player object
	 *
	 *First and second digit shows level of the hand
	 *Third and Fourth digits show strength of the main card in the hand (Usually Highest)
	 *Fifth and Sixth digit shows strength of the secondary card in the hand (Usually Secondary pair)
	 *Decimals shows strength of filler cards to complete the hand of 5
	 *Royal Flush 		= 200 000
	 *Straight Flush	= 180 000
	 *Four of a Kind	= 160 000
	 *Full House 		= 140 000
	 *Flush				= 120 000
	 *Straight			= 100 000
	 *Three of a Kind	=  80 000
	 *Two Pairs			=  60 000
	 *Pairs				=  40 000
	 *High Card			=  20 000
	 */
	public DynamicCardArray getBestHand(Player p){
		DynamicCardArray wholeHand = new DynamicCardArray();
		DynamicCardArray bestHand = null;
		for(int i = 0; i < cards.getHandSize(); i++)
			wholeHand.add(cards.getCard(i));
		for(int i = 0; i < p.getHandSize(); i++)
			wholeHand.add(p.getCard(i));
		wholeHand.sort();
		boolean hasFlush = checkFlush(wholeHand);
		Suit mostFrequentSuit = findMostFrequentSuit(wholeHand);
		if(hasFlush){
			bestHand = findRoyalFlush(wholeHand, mostFrequentSuit, p);
			if(bestHand == null)
				bestHand = findStraightFlush(wholeHand, mostFrequentSuit, p);
		}
		if(bestHand == null)
			bestHand = findFourOfAKind(wholeHand, p);
		if(bestHand == null)
			bestHand = findFullHouse(wholeHand, p);
		if(hasFlush && bestHand == null){
			bestHand = findFlush(wholeHand, mostFrequentSuit, p);
			return bestHand;
		}
		if(bestHand == null)
			bestHand = findStraight(wholeHand, p);
		if(bestHand == null)
			bestHand = findThreeOfAKind(wholeHand, p);
		if(bestHand == null)
			bestHand = findTwoPairs(wholeHand, p);
		if(bestHand == null)
			bestHand = findPair(wholeHand, p);
		if(bestHand == null){
			p.setHandStrength(20000);
			bestHand = new DynamicCardArray();
		}
		if(bestHand.size() < 5)
			bestHand = getHighCard(bestHand, wholeHand, p);
		return bestHand;
	}
	
	private static boolean checkFlush(DynamicCardArray h){
		for(Suit s : Suit.values())
			if(h.countSuit(s) >= 5)
				return true;
		return false;
	}
	
	//Adds the highest cards from allCards that arent in selectedCards
	private static DynamicCardArray getHighCard(DynamicCardArray selectedCards, DynamicCardArray allCards, Player p){
		DynamicCardArray toAdd = new DynamicCardArray();
		for(int i = allCards.size() - 1; i >= 0; i--)
			if(!selectedCards.contains(allCards.get(i))){
				toAdd.add(allCards.get(i));
				if(selectedCards.size() + toAdd.size() == 5){
					break;
				}
			}
		p.changeStrengthBy(getTieBreakerStrength(toAdd));
		selectedCards.merge(toAdd);
		return selectedCards;
	}
	
	private static DynamicCardArray findRoyalFlush(DynamicCardArray h, Suit mostFrequentSuit, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = 0; i < h.size(); i++){
			if(h.get(i).getStrengthValue() >= 8 && h.get(i).getStrengthValue() <= 12 && h.get(i).getSuit() == mostFrequentSuit)
				relevant.add(h.get(i));
		}
		if(relevant.size() == 5){
			p.setHandStrength(200000);
			return relevant;
		}
		return null;
	}
	
	private static DynamicCardArray findStraightFlush(DynamicCardArray h, Suit mostFrequentSuit, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		DynamicCardArray flush = new DynamicCardArray();
		for(int i = 0; i < h.size(); i++)
			if(h.get(i).getSuit() == mostFrequentSuit)
				flush.add(h.get(i));

		relevant = findStraight(flush, p);
		if(relevant != null){
			p.setHandStrength(180000 + relevant.get(0).getStrengthValue() * 100);
			return relevant;
		}
		return null;
	}

	private static DynamicCardArray findFourOfAKind(DynamicCardArray h, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = h.size() - 2; i >= 0; i--){
			if(h.get(i + 1).getStrengthValue() == h.get(i).getStrengthValue()){
				relevant.add(h.get(i + 1));
				if(relevant.size() == 3){
					relevant.add(h.get(i));
					p.setHandStrength(160000 + relevant.get(0).getStrengthValue() * 100);
					return relevant;
				}
			}else{
				relevant.empty();
			}
		}
		return null;
	}

	private static DynamicCardArray findFullHouse(DynamicCardArray h, Player p){
		DynamicCardArray relevant = findThreeOfAKind(h, p);
		if(relevant != null){
			for(int i = h.size() - 2; i >= 0; i--){
				if(h.get(i + 1).getStrengthValue() == h.get(i).getStrengthValue() && !(relevant.contains(h.get(i + 1)))){
					relevant.add(h.get(i + 1));
					relevant.add(h.get(i));
					p.setHandStrength(140000 + relevant.get(0).getStrengthValue() * 100 + relevant.get(3).getStrengthValue());
					return relevant;
				}
			}
		}
		return null;
	}

	private static DynamicCardArray findFlush(DynamicCardArray h, Suit mostFrequentSuit, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = h.size() - 1; i >= 0; i--)
			if(h.get(i).getSuit() == mostFrequentSuit){
				relevant.add(h.get(i));
				if(relevant.size() == 5){
					p.setHandStrength(120000 + getTieBreakerStrength(relevant));
					return relevant;
				}
			}
		return null;
	}

	private static DynamicCardArray findStraight(DynamicCardArray h, Player p){
		//If an ace is found adds the LOWACE as well in the possible cards
		DynamicCardArray relevant = new DynamicCardArray();
		DynamicCardArray allCards = h.copy();
		for(Suit s : Suit.values())
			if(allCards.contains(new Card(Value.ACE, s)))
				allCards.add(new Card(Value.LOWACE, s));
			
		allCards.sort();
			
		for(int i = allCards.size() - 2; i >= 0; i--){
			if(allCards.get(i + 1).getStrengthValue() - 1 == allCards.get(i).getStrengthValue()){
				relevant.add(allCards.get(i + 1));
				if(relevant.size() == 4){
					relevant.add(allCards.get(i));
					p.setHandStrength(100000 + relevant.get(0).getStrengthValue() * 100);
					return relevant;
				}
			}else if(allCards.get(i + 1).getStrengthValue() != allCards.get(i).getStrengthValue()){
				relevant.empty();
			}
		}
		return null;
	}

	private static DynamicCardArray findThreeOfAKind(DynamicCardArray h, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = h.size() - 2; i >= 0; i--){
			if(h.get(i + 1).getStrengthValue() == h.get(i).getStrengthValue()){
				relevant.add(h.get(i + 1));
				if(relevant.size() == 2){
					relevant.add(h.get(i));
					p.setHandStrength(80000 + relevant.get(0).getStrengthValue() * 100);
					return relevant;
				}
			}else{
				relevant.empty();
			}
		}
		return null;
	}

	private static DynamicCardArray findTwoPairs(DynamicCardArray h, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = h.size() - 2; i >= 0; i--){
			if(h.get(i + 1).getStrengthValue() == h.get(i).getStrengthValue()){
				relevant.add(h.get(i + 1));
				relevant.add(h.get(i));
				if(relevant.size() == 4){
					p.setHandStrength(60000 + relevant.get(0).getStrengthValue() * 100 + relevant.get(2).getStrengthValue());
					return relevant;
				}
			}
		}
		return null;
	}

	private static DynamicCardArray findPair(DynamicCardArray h, Player p){
		DynamicCardArray relevant = new DynamicCardArray();
		for(int i = h.size() - 2; i >= 0; i--){
			if(h.get(i + 1).getStrengthValue() == h.get(i).getStrengthValue()){
				relevant.add(h.get(i + 1));
				relevant.add(h.get(i));
				p.setHandStrength(40000 + relevant.get(0).getStrengthValue() * 100);
				return relevant;
			}
		}
		return null;
	}
	
	private static Suit findMostFrequentSuit(DynamicCardArray h) {
		Suit[] suits = Suit.values();
		Suit mostFrequentSuit			= null;
		int maxOccurences = 0, occurences 	= 0;
		
		for(int i = 0; i < suits.length; i++) {
			occurences = 0;
			occurences = h.countSuit(suits[i]);
			if(occurences >= maxOccurences) {
				maxOccurences = occurences;
				mostFrequentSuit = suits[i];
			}
		}
		return mostFrequentSuit;
	}
	
	private static double getTieBreakerStrength(DynamicCardArray h){
		double toReturn = 0;
		for(int i = h.size() - 1; i >= 0; i--){
			toReturn += h.get(i).getStrengthValue() / Math.pow(10, 2 + (h.size() - 1 - i) * 2);
		}
		return toReturn;
	}
	
}