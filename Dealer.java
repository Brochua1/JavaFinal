public class Dealer{
	private static final int HAND_SIZE = 5;
	private Hand hand;
	private String name;
	
	//constructor
	public Dealer(String name){
		this.name = name;
		this.hand = new Hand();
	}
	
	//getters
	public String getName(){
		return this.name;
	}
	
	public Card getCard(int i){
		return this.hand.getCard(i);
	}
	
	public int getHandSize(){
		return this.hand.getHandSize();
	}
	
	//calls checkStrength on its hand and returns its value
	public double getHandStrength(){
		return hand.checkStrength();
	}
	
	//Custom methods
	public void drawHand(Deck origin){
		for(int i = 0; i < HAND_SIZE; i++)
			hand.addCard(origin.drawTopCard());
	}
	
	//Sorts dealer's hand
	public void sortHand(){
		this.hand.sort();
	}
	
	//Empties the dealer's hand
	public void dropHand(){
		this.hand.empty();
	}
	
	//Replaces the card at a given index and returns the card it overwrote
	public Card exchangeCard(Card c, int i){
		Card toReturn = this.hand.getCard(i);
		this.hand.replaceCardAt(c, i);
		return toReturn;
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		this.hand.flipCardAt(i);
	}
	
	//Flips all cards in hand
	public void flipAllCards(){
		this.hand.flipAllCards();
	}
	
	public String toString(){
		return "Dealer: " + this.name + "\n" + hand.toString();
	}
}