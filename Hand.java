
public class Hand {
	private DynamicCardArray cards;
	
	public Hand() {
		this.cards = new DynamicCardArray();
	}
	
	//returns card at specified index
	public Card getCard(int i) {
		return this.cards.get(i);
	}
	
	public int getHandSize(){
		return cards.size();
	}
	
	//removes card at a given index and shift all other cards to fill in the gap
	public void removeCard(int i) {
		this.cards.remove(i);
	}
	
	//adds a card to the end of the hand
	public void addCard(Card c) {
		this.cards.add(c);
	}
	
	//Empties hand
	public void empty() {
		this.cards.empty();
	}
	
	//Puts a given card at a given index
	public void replaceCardAt(Card c, int i){
		cards.set(c, i);
	}
	
	//sorts the cards by strength
	public void sort(){
		this.cards.sort();
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		this.cards.flipCardAt(i);
	}
	
	//Flips all cards in hand
	public void flipAllCards(){
		for(int i = 0; i < this.cards.size(); i++)
			this.cards.flipCardAt(i);
	}
	
	
	//Returns an int for the Strength of the hand 
	/*
	 *First digit shows level of the hand
	 *Second digit shows strength of the most common card in that hand
	 *Third digit shows strength of the 2nd most common card in that hand
	 *5 of a kind 	= 160
	 *4 of a kind 	= 80
	 *Full house 	= 60
	 *Triple 		= 40
	 *Two Pairs		= 30
	 *Pair			= 20
	 *High Card		= 10
	 */
	public double checkStrength() {
		Picture mostFrequentPic 		= findMostFrequentPic(null);
		int highestPicCount				= this.cards.countPictures(mostFrequentPic);
		boolean secondaryPair 			= false;
		Picture secondMostFrequentPic;
		double highCardStrength;
		//5 of a kind
		if(highestPicCount == 5)
			return 160 + mostFrequentPic.getStrength();
		
		secondMostFrequentPic 			= findMostFrequentPic(mostFrequentPic);
		secondaryPair 					= (this.cards.countPictures(secondMostFrequentPic) == 2);
		highCardStrength				= (secondMostFrequentPic.getStrength() / 10.0);
		//4 of a kind
		if(highestPicCount == 4)
			return 80 + mostFrequentPic.getStrength();
		else if(highestPicCount ==  3){
			//Full house
			if(secondaryPair)
				return 60 + mostFrequentPic.getStrength() + highCardStrength;
			//3 of a kind
			else
				return 40 + mostFrequentPic.getStrength() + highCardStrength;
		}else if(highestPicCount == 2){
			//2 pairs
			if(secondaryPair)
				return 30 + mostFrequentPic.getStrength() + highCardStrength;
			//pair
			else
				return 20 + mostFrequentPic.getStrength() + highCardStrength;
		}else
			//High Card
			return 10 + mostFrequentPic.getStrength() + highCardStrength;
	}
	
	//Returns the picture that is found most often in the hand
	private Picture findMostFrequentPic(Picture toIgnore) {
		Picture[] pics = Picture.values();
		Picture mostFrequentPicture			= null;
		int maxOccurences = 0, occurences 	= 0;
		
		for(int i = 0; i < pics.length; i++) {
			occurences = 0;
			if(pics[i] != toIgnore)
				occurences = cards.countPictures(pics[i]);
			if(occurences >= maxOccurences) {
				maxOccurences = occurences;
				mostFrequentPicture = pics[i];
			}
		}
		return mostFrequentPicture;
	}

	public String toString() {
		return this.cards.toString();
	}
}
