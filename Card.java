public class Card {
	private final String CARD_BACK = "XXXXX";
    private Picture picture;
    private int strength;
	private boolean isFaceDown;

    //Constructor setting the picture and strength based on the picture
    Card(Picture pic){
        this.picture = pic;
        this.strength = pic.getStrength();
		this.isFaceDown = false;
    }

    //getters
    public Picture getPicture() {
        return this.picture;
    }
    
    public int getStrength() {
		return this.strength;
	}
	
	public boolean getIsFaceDown(){
		return this.isFaceDown;
	}
	
	//Flips the card, if it is face up it makes it face down and vice versa
	public void flipCard(){
		this.isFaceDown = !this.isFaceDown;
	}

    //toString override
    public String toString(){
		if (isFaceDown)
			return CARD_BACK;
        return this.picture.toString();
    }
}
