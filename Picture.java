public enum Picture {
	CLOUD(0){
        public String toString() {
            return "Cloud";
        }
    },
	MUSHROOM(1){
        public String toString() {
            return "Mushroom";
        }
    },
	FLOWER(2){
        public String toString(){
            return "Flower";
        }
    },
	LUIGI(3){
        public String toString() {
            return "Luigi";
        }
    },
	MARIO(4){
        public String toString() {
            return "Mario";
        }
    },
	STAR(5){
        public String toString() {
            return "Star";
        }
    };
	
	private final int strength;
	
	private Picture(int strength){
		this.strength = strength;
	}
	
	public int getStrength(){
		return this.strength;
	}
}
