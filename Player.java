public class Player{
	private static final int DEFAULT_BAL = 10;
	private static final int HAND_SIZE = 5;
	private int balance;
	private int stars;
	private Hand hand;
	private String name;
	
	//constructor
	public Player(String name){
		this.name = name;
		this.hand = new Hand();
		this.balance = DEFAULT_BAL;
		this.stars = 0;
	}
	
	//getters
	public int getBalance(){
		return this.balance;
	}

	public String getName(){
		return this.name;
	}
	
	public Card getCard(int i){
		return this.hand.getCard(i);
	}
	
	public int getHandSize(){
		return this.hand.getHandSize();
	}
	
	//calls checkStrength on its hand and returns its value
	public double getHandStrength(){
		return hand.checkStrength();
	}
	
	//adds a value to the balance as long as it wouldnt make it negative
	public void changeBalanceBy(int toAdd){
		if(this.balance + toAdd < 0)
			throw new IllegalArgumentException("Balance cannot be negative");
		this.balance += toAdd;
	}
	
	//Increments the stars
	public void incrementStars(){
		this.stars++;
	}
	
	//Decrements the stars (If stars was to go below 0 nothing happens)
	public void decrementStars(){
		if(this.stars > 0)
			this.stars--;
	}
	
	//Calculates the minimum bet by the player based on their stars
	//minimum bet is equal to the stars divided by 5 plus 1 up to 4
	public int getMinBet(){
		int bet = (this.stars / 5 + 1 <= 4) ? this.stars / 5 + 1: 4;
		return bet;
	}	
	
	//Custom methods
	//Draws HAND_SIZE cards from origin deck
	public void drawHand(Deck origin){
		for(int i = 0; i < HAND_SIZE; i++)
			hand.addCard(origin.drawTopCard());
	}
	
	//shorts player's hand
	public void sortHand(){
		this.hand.sort();
	}
	
	//Empties the player's hand
	public void dropHand(){
		this.hand.empty();
	}
	
	//Replaces the card at a given index with a new one
	public void exchangeCard(Card c, int i){
		this.hand.replaceCardAt(c, i);
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		this.hand.flipCardAt(i);
	}
	
	//Flips all cards in hand
	public void flipAllCards(){
		this.hand.flipAllCards();
	}
	
	//Returns a string representation of a player's hand object with the index of each card displayed next to it
	private String handToNumberedString(){
		String toReturn = "";
		int count = 0;
		for(String s : hand.toString().split("\n")){
			toReturn += count + " > " + s + "\n";
			count++;
		}
		return toReturn;
	}
	
	public String toString(){
		return this.name + " - " + this.balance + " Coins " + this.stars + " Stars\n" + this.handToNumberedString();
	}
}