public class DynamicCardArray{
    private Card[] array;
	private int size;

	//Creates the array with a very large size and sets size to 0
	public DynamicCardArray(){
		this.array = new Card[10000];
		this.size = 0;
	}
	
	//Add a given value to the end of array, updating the size
	public void add(Card value){
		if(this.size == this.array.length)
			this.doubleArrayLength();
		this.array[this.size] = value;
		this.size++;
	}
	
	//Return the value for a given index of the array
	public Card get(int i){
		if(i < this.size && i >= 0)
			return this.array[i];
		throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
	}
	
	//set the card at a given index to a given card
	public void set(Card c, int i){
		if(!(i < this.size && i >= 0))
			throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
		this.array[i] = c;
	}
	
	//remove card at a given index and reduce the size accordingly
	public void remove(int i) {
		this.size--;
		for(int j = i; j < this.size; j++)
			this.array[j] = this.array[j + 1];
	}
	
	//Flips the card at given index
	public void flipCardAt(int i){
		if(!(i < this.size && i >= 0))
			throw new IllegalArgumentException("Index : " + i + " is invalid expected between " + this.size + " and " + 0);
		this.array[i].flipCard();
	}
	
	//Return the size of the array
	public int size(){
		return this.size;
	}
	
	//Empties the array
	public void empty() {
		for(int i = 0; i < size; i++)
			this.array[i] = null;
		this.size = 0;
	}
	
	//counts how many occurrences of a given picture there are
	public int countPictures(Picture p) {
		int count = 0;
		for(int i = 0; i < this.size; i++) {
			if(array[i].getPicture() == p)
				count++;
		}
		return count;
	}
	
	//Checks if a value is contained in the array
	public boolean contains(Card p){
		for(int i = 0; i < this.size; i++)
			if(this.array[i].getPicture() == p.getPicture())
				return true;
		return false;
	}
	
	//Doubles the length of the array
	private void doubleArrayLength(){
		Card[] newArr = new Card[this.array.length * 2];
		for(int i = 0; i < this.size; i++){
			newArr[i] = array[i];
		}
		
		this.array = newArr;
	}
	
	public void sort(){
		int posOfMin;
		Card temp = null;

		for(int j = 0; j < this.size - 1; j++){
			posOfMin = j;
			for(int i = j + 1; i < this.size; i++)
				if (this.array[i].getPicture().getStrength() < this.array[posOfMin].getPicture().getStrength())
					posOfMin = i;

			temp = this.array[j];
			this.array[j] = this.array[posOfMin];
			this.array[posOfMin] = temp;
		}
	}
	
	public String toString(){
        String toReturn = "";
        for(int i = 0; i < this.size; i++)
            toReturn += this.array[i] + "\n";
        return toReturn;
    }
}